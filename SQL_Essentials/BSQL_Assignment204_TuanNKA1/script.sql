USE master
GO

IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'BSQL_Assignment204_TuanNKA1'
)
	DROP DATABASE BSQL_Assignment204_TuanNKA1
GO


-- Create database ASQL_Assignment204_TuanNKA1
CREATE DATABASE BSQL_Assignment204_TuanNKA1

GO

USE BSQL_Assignment204_TuanNKA1

GO

-- 1. Create your tables

-- Create the Movie table
CREATE TABLE Movie (
    MovieID INT PRIMARY KEY IDENTITY(1,1),
    MovieName VARCHAR(255) NOT NULL,
    Duration INT CHECK (Duration >= 60),
    Genre smallint CHECK (Genre BETWEEN 1 AND 8),
    Director VARCHAR(255) NOT NULL,
    BoxOfficeMoney MONEY,
    Comments VARCHAR(MAX),
    ImageLink VARCHAR(255) UNIQUE
);

-- Create the Actor table
CREATE TABLE Actor (
    ActorID INT PRIMARY KEY IDENTITY(1,1),
    ActorName VARCHAR(255) NOT NULL,
    Age INT NOT NULL,
    AvgMovieSalary MONEY NOT NULL,
    Nationality VARCHAR(255) NOT NULL
);

-- Create the ActedIn table with FOREIGN KEYS
CREATE TABLE ActedIn (
    MovieID INT,
    ActorID INT,
    PRIMARY KEY (MovieID, ActorID),
    FOREIGN KEY (MovieID) REFERENCES Movie(MovieID),
    FOREIGN KEY (ActorID) REFERENCES Actor(ActorID)
);

-- 2. Populate tables
-- Populate the Movie table
INSERT INTO Movie (MovieName, Duration, Genre, Director, BoxOfficeMoney, Comments, ImageLink)
VALUES
    ('Movie 1', 120, 1, 'Director 1', 1000000, 'Comment 1', 'link1.jpg'),
    ('Movie 2', 90, 3, 'Director 2', 800000, 'Comment 2', 'link2.jpg'),
    ('Movie 3', 110, 2, 'Director 3', 1200000, 'Comment 3', 'link3.jpg'),
    ('Movie 4', 140, 5, 'Director 4', 1600000, 'Comment 4', 'link4.jpg'),
    ('Movie 5', 95, 1, 'Director 1', 900000, 'Comment 5', 'link5.jpg');

-- Populate the Actor table
INSERT INTO Actor (ActorName, Age, AvgMovieSalary, Nationality)
VALUES
    ('Actor 1', 45, 500000, 'Nationality 1'),
    ('Actor 2', 55, 600000, 'Nationality 2'),
    ('Actor 3', 60, 700000, 'Nationality 3'),
    ('Actor 4', 40, 550000, 'Nationality 4'),
    ('Actor 5', 50, 650000, 'Nationality 5');

-- Populate the ActedIn table
INSERT INTO ActedIn (MovieID, ActorID)
VALUES
    (1, 1),
    (2, 1),
    (1, 2),
    (3, 2),
    (2, 3);


-- 3. Query tables

-- Write a query to retrieve all the data in the Actor table for actors that are older than 50.
select * 
from Actor
where Age > 50

-- Write a query to retrieve all actor names and average salaries from ACTOR and sort the results by average salary.
select ActorName, AvgMovieSalary
from Actor
order by AvgMovieSalary

-- Using an actor name from your table, write a query to retrieve the names of all the movies that the actor has acted in.

-- each actor id, list name movie which acted
select ActorID, string_agg(MovieName, ',') MovieNames
from ActedIn
    join Movie on ActedIn.MovieID = Movie.MovieID
group by ActorID