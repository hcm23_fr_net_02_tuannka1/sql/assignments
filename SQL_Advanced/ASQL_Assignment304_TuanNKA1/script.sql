IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'ASQL_Assignment304_TuanNKA1'
)
	DROP DATABASE ASQL_Assignment304_TuanNKA1
GO


-- Create database ASQL_Assignment304_TuanNKA1
CREATE DATABASE ASQL_Assignment304_TuanNKA1

GO

USE ASQL_Assignment304_TuanNKA1

GO

-------------------- Create table

/****** Object:  Table [dbo].[EMPMAJOR]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPMAJOR](
	[emp_no] [char](6) NOT NULL,
	[major] [char](3) NOT NULL,
	[major_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Major] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[major] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

-- INSERT statements for the EMPMAJOR table
INSERT INTO [dbo].[EMPMAJOR] ([emp_no], [major], [major_name])
VALUES
	('000001', 'CSI', 'Mathematics'),
    ('000001', 'MAT', 'Mathematics'),
    ('000002', 'CSI', 'Computer Science'),
    ('000003', 'MAT', 'Mathematics'),
    ('000004', 'PHY', 'Physics'),
    ('000005', 'CSI', 'Computer Science'),
    ('000006', 'PHY', 'Physics'),
    ('000007', 'MAT', 'Mathematics'),
    ('000008', 'BIO', 'Biology');

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMP]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMP](
	[emp_no] [char](6) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[dept_no] [char](3) NOT NULL,
	[job] [varchar](50) NULL,
	[salary] [money] NOT NULL,
	[bonus] [money] NULL,
	[ed_level] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

-- INSERT statements for the EMP table
INSERT INTO [dbo].[EMP] ([emp_no], [last_name], [first_name], [dept_no], [job], [salary], [bonus], [ed_level])
VALUES
    ('000001', 'Smith', 'John', 'D01', 'Manager', 60000.00, 5000.00, 4),
    ('000002', 'Johnson', 'Susan', 'D02', 'Engineer', 55000.00, 3000.00, 3),
    ('000003', 'Williams', 'David', 'D01', 'Analyst', 52000.00, 2500.00, 3),
    ('000004', 'Jones', 'Mary', 'D03', 'Manager', 61000.00, 5200.00, 4),
    ('000005', 'Brown', 'Jennifer', 'D02', 'Technician', 48000.00, 2000.00, 2),
    ('000006', 'Davis', 'Michael', 'D03', 'Engineer', 56000.00, 3200.00, 3),
    ('000007', 'Miller', 'Laura', 'D02', 'Analyst', 53000.00, 2800.00, 3),
    ('000008', 'Wilson', 'Robert', 'D01', 'Technician', 49000.00, 2100.00, 2),
	('000009', 'Rick', 'Hollan', 'D01', 'Engineer', 61000.00, 2100.00, 2);

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DEPT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DEPT](
	[dept_no] [char](3) NOT NULL,
	[dept_name] [varchar](50) NOT NULL,
	[mgn_no] [char](6) NULL,
	[admr_dept] [char](3) NOT NULL,
	[location] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[dept_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[dept_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

-- INSERT statements for the DEPT table
INSERT INTO [dbo].[DEPT] ([dept_no], [dept_name], [mgn_no], [admr_dept], [location])
VALUES
    ('D01', 'HR', '000001', 'D01', 'New York'),
    ('D02', 'IT', '000004', 'D02', 'San Francisco'),
    ('D03', 'Finance', '000005', 'D03', 'Los Angeles'),
    ('D04', 'Marketing', '000002', 'D04', 'Chicago');

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMPPROJACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPPROJACT](
	[emp_no] [char](6) NOT NULL,
	[proj_no] [char](6) NOT NULL,
	[act_no] [int] NOT NULL,
 CONSTRAINT [PK_EPA] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[proj_no] ASC,
	[act_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

-- INSERT statements for the EMPPROJACT table
INSERT INTO [dbo].[EMPPROJACT] ([emp_no], [proj_no], [act_no])
VALUES
    ('000001', 'P001', 101),
    ('000001', 'P002', 102),
    ('000002', 'P003', 103),
    ('000002', 'P002', 102),
    ('000003', 'P001', 101),
    ('000004', 'P004', 104),
    ('000005', 'P001', 101),
    ('000006', 'P003', 103);

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACT](
	[act_no] [int] NOT NULL,
	[act_des] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[act_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

-- INSERT statements for the ACT table
INSERT INTO [dbo].[ACT] ([act_no], [act_des])
VALUES
    (101, 'Activity 1'),
    (102, 'Activity 2'),
    (103, 'Activity 3'),
    (104, 'Activity 4');

GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_Dept]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[DEPT]  WITH CHECK ADD  CONSTRAINT [FK_Dept] FOREIGN KEY([mgn_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[DEPT] CHECK CONSTRAINT [FK_Dept]
GO
/****** Object:  ForeignKey [FK__EMP__dept_no__3E52440B]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMP]  WITH CHECK ADD FOREIGN KEY([dept_no])
REFERENCES [dbo].[DEPT] ([dept_no])
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_Major]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPMAJOR]  WITH CHECK ADD  CONSTRAINT [FK_Major] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPMAJOR] CHECK CONSTRAINT [FK_Major]
GO
/****** Object:  ForeignKey [FK_EPA1]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA1] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA1]
GO
/****** Object:  ForeignKey [FK_EPA2]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA2] FOREIGN KEY([act_no])
REFERENCES [dbo].[ACT] ([act_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA2]
GO

--------------------------- sumary
-- 

---------------------------

-------------------- 1.	Add at least 8 records into each created tables. --------------------
-- above

-------------------- 2.	Find EMP who are currently working on a project or projects. EMP working on projects will have a row(s) on the EMPPROJACT table. --------------------

select 	emp_no, last_name
from EMP
where emp_no in (
	select emp_no
	from EMPPROJACT
)


-------------------- 3.	Find all EMP who major in math (MAT) and computer science (CSI). --------------------

select emp_no, last_name
from EMP
where emp_no in (
	select emp_no
	from EMPMAJOR
	where major in  ('MAT', 'CSI')
	group by emp_no
	having count(major) = 2
)

-------------------- 4.	Find EMP who work on all activities between 90 and 110 --------------------

select emp_no, last_name
from EMP
where emp_no in (
	select emp_no
	from EMPPROJACT
	where act_no between 90 and 110
	group by emp_no
	having count(distinct act_no) = (110 - 90 + 1)
)

-------------------- 5.	Provide a report of EMP with employee detail information along with department aggregate information.  --------------------

select E.emp_no, E.last_name, E.first_name, E.salary, E.dept_no, DEPT_AVG.DEPT_AVG_SAL
from EMP E
	join (
		select dept_no, avg(salary) DEPT_AVG_SAL
		from EMP
		group by dept_no
	) DEPT_AVG
	ON E.dept_no = DEPT_AVG.dept_no
;

;with DEPT_AVG as (
	select dept_no, avg(salary) DEPT_AVG_SAL
	from EMP
	group by dept_no	
)
select E.emp_no, E.last_name, E.first_name, E.salary, E.dept_no, DEPT_AVG.DEPT_AVG_SAL
from EMP E
	join DEPT_AVG
	ON E.dept_no = DEPT_AVG.dept_no
;

-------------------- 6.	Use CTE technique to provide a report of EMP whose education levels are higher than the average education level of their respective department. --------------------

;with DEPT_AVG as (
	select dept_no, avg(ed_level) DEPT_AVG_LEVEL
	from EMP
	group by dept_no
)
select emp_no, last_name
from EMP E
where ed_level > (
	select DEPT_AVG_LEVEL
	from DEPT_AVG
	where dept_no = E.dept_no
)
-------------------- 7.	Return the department number, department name and the total payroll for the department that has the highest payroll. Payroll will be defined as the sum of all salaries and bonuses for the department. --------------------

;with PayRollDept as (
	select  dept_no, (sum(salary) + sum(bonus)) payroll
	from EMP
	group by dept_no
)
select D.dept_no, D.dept_name, HPD.payroll 
from Dept D
	join 
	(select top 1 dept_no, payroll from PayRollDept order by payroll desc) HPD
	on D.dept_no = HPD.dept_no

--------------------  8.	Return the EMP with the top 5 salaries.--------------------

-- Could be 5 employees with different salaries.
select *
from EMP
order by salary desc
go

select top 5 emp_no, last_name, salary, row_number() over (order by salary desc) rank
from EMP
go
-- Could be many employees having the same salaries.
