-- Create database ASQL_Assignment301_TuanNKA1
CREATE DATABASE ASQL_Assignment301_TuanNKA1

GO

USE ASQL_Assignment301_TuanNKA1

GO

-- Create table Employee, Status = 1: are working
CREATE TABLE [dbo].[Employee](
	[EmpNo] [int] NOT NULL
,	[EmpName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[BirthDay] [datetime] NOT NULL
,	[DeptNo] [int] NOT NULL
, 	[MgrNo] [int]
,	[StartDate] [datetime] NOT NULL
,	[Salary] [money] NOT NULL
,	[Status] [int] NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
,	[Level] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE Employee 
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Level] 
	CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO
ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Status] 
	CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))

GO
ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30) 
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] 
ADD CONSTRAINT chk_Email1 UNIQUE(Email)

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status

GO
CREATE TABLE [dbo].[Skill](
	[SkillNo] [int] IDENTITY(1,1) NOT NULL
,	[SkillName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)

GO
CREATE TABLE [dbo].[Department](
	[DeptNo] [int] IDENTITY(1,1) NOT NULL
,	[DeptName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)

GO
CREATE TABLE [dbo].[Emp_Skill](
	[SkillNo] [int] NOT NULL
,	[EmpNo] [int] NOT NULL
,	[SkillLevel] [int] NOT NULL
,	[RegDate] [datetime] NOT NULL
,	[Description] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  
ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_2] FOREIGN KEY ([EmpNo])
REFERENCES Employee([EmpNo])

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_3] FOREIGN KEY ([SkillNo])
REFERENCES Skill([SkillNo])

GO

-------------------- 1. Add at least 8 records into each created tables. --------------------

-- Insert data into the Department table
INSERT INTO Department (DeptName, Note)
VALUES
    ('HR', 'Human Resources Department'),
    ('IT', 'Information Technology Department'),
    ('Sales', 'Sales Department'),
    ('Finance', 'Finance Department'),
    ('Marketing', 'Marketing Department'),
    ('Engineering', 'Engineering Department'),
    ('Operations', 'Operations Department'),
    ('Legal', 'Legal Department');

-- Insert data into the Skill table
INSERT INTO Skill (SkillName, Note)
VALUES
    ('SQL', 'Structured Query Language'),
    ('C++', 'C++ Programming Language'),
    ('.NET', '.NET Programming Language'),
    ('Python', 'Python Programming Language'),
    ('Project Management', 'Project Management Skills'),
    ('Data Analysis', 'Data Analysis Skills'),
    ('Communication', 'Communication Skills'),
    ('Problem Solving', 'Problem Solving Skills');

-- Insert data into the Employee table
INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email)
VALUES
    (1, 'John Smith', '1980-01-15', 1, NULL, '2005-05-10', 60000, 1, NULL, 5, 'john.smith@example.com'),
    (2, 'Jane Doe', '1985-03-20', 2, 1, '2010-02-18', 75000, 1, NULL, 6, 'jane.doe@example.com'),
    (3, 'Robert Johnson', '1990-07-12', 1, NULL, '2012-09-30', 55000, 1, NULL, 4, 'robert.johnson@example.com'),
    (4, 'Mary Williams', '1988-11-28', 3, 2, '2008-08-05', 68000, 1, NULL, 5, 'mary.williams@example.com'),
    (5, 'David Lee', '1992-05-03', 2, 1, '2015-04-22', 72000, 1, NULL, 4, 'david.lee@example.com'),
    (6, 'Jennifer Brown', '1987-09-08', 4, 3, '2007-03-15', 58000, 1, NULL, 3, 'jennifer.brown@example.com'),
    (7, 'Michael Clark', '1991-02-19', 1, NULL, '2019-11-12', 63000, 1, NULL, 4, 'michael.clark@example.com'),
    (8, 'Susan Wilson', '1983-04-25', 3, 2, '2023-06-08', 71000, 1, NULL, 5, 'susan.wilson@example.com');

-- Insert data into the Emp_Skill table
INSERT INTO Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description)
VALUES
    (3, 1, 5, '2020-01-15', '.NET proficiency'),
	(2, 1, 4, '2019-09-23', 'C++ development skills'),
    (2, 2, 4, '2020-02-20', 'C++ development skills'),
    (3, 3, 3, '2019-12-10', '.NET knowledge'),
    (4, 4, 5, '2021-03-05', 'Python scripting skills'),
    (5, 5, 4, '2021-04-12', 'Project management certification'),
    (6, 6, 3, '2020-07-22', 'Data analysis skills'),
    (7, 7, 4, '2022-05-18', 'Excellent communication skills')

-------------------- 2.	Specify the name, email and department name of the employees that have been working at least six months. --------------------
SELECT E.EmpName, E.Email, D.DeptName
FROM Employee E
	JOIN Department D ON E.DeptNo = D.DeptNo
WHERE DATEDIFF(MONTH, E.StartDate, GETDATE()) >= 6;

SELECT E.EmpName, E.Email, D.DeptName
FROM 
	(
		SELECT E.EmpName, E.DeptNo, E.Email 
		FROM Employee E 
		WHERE DATEDIFF(MONTH, E.StartDate, GETDATE()) >= 6
	) E
	JOIN Department D ON E.DeptNo = D.DeptNo
;

-------------------- 3.	Specify the names of the employees whore have either 'C++' or '.NET' skills. --------------------
SELECT DISTINCT E.EmpName
FROM Employee E
	JOIN Emp_Skill ES ON E.EmpNo = ES.EmpNo
	JOIN Skill S ON ES.SkillNo = S.SkillNo
WHERE S.SkillName IN ('C++', '.NET');

SELECT E.EmpName
FROM Employee E
WHERE E.EmpNo IN (
	SELECT EmpNo
	FROM Emp_Skill ES
	WHERE ES.SkillNo IN (
		SELECT S.SkillNo
		FROM Skill S
		WHERE S.SkillName IN ('C++', '.NET')
	)
);

-------------------- 4.	List all employee names, manager names, manager emails of those employees. --------------------
SELECT
    E.EmpName AS EmployeeName,
    M.EmpName AS ManagerName,
    M.Email AS ManagerEmail
FROM
    Employee E
	LEFT JOIN Employee M ON E.MgrNo = M.EmpNo;

-------------------- 5.	Specify the departments which have >=2 employees, print out the list of departments� employees right after each department. --------------------
SELECT D.DeptName
FROM Department D
	JOIN Employee E ON D.DeptNo = E.DeptNo
GROUP BY D.DeptName
HAVING COUNT(E.EmpNo) >= 2;

SELECT D.DeptName
FROM Department D
WHERE D.DeptNo IN (
	SELECT E.DeptNo
	FROM Employee E
	GROUP BY E.DeptNo
	HAVING COUNT(*) >=2
);

SELECT
    D.DeptName,
    STRING_AGG(Trim(E.EmpName), ',') AS Employees
FROM Department D
	JOIN Employee E ON D.DeptNo = E.DeptNo
GROUP BY D.DeptName
HAVING COUNT(E.EmpNo) >= 2;

;WITH DEPARTMORETHAN2EMPS AS (
	SELECT D.DeptName, D.DeptNo
	FROM Department D
	WHERE D.DeptNo IN (
		SELECT E.DeptNo
		FROM Employee E
		GROUP BY E.DeptNo
		HAVING COUNT(*) >=2
	)
)
SELECT
    D.DeptName,
    STRING_AGG(Trim(E.EmpName), ',') AS Employees
FROM DEPARTMORETHAN2EMPS D
	JOIN Employee E ON D.DeptNo = E.DeptNo
GROUP BY D.DeptName
HAVING COUNT(E.EmpNo) >= 2;

-------------------- 6.	List all name, email and number of skills of the employees and sort ascending order by employee�s name. --------------------
SELECT
    E.EmpName,
    E.Email,
    COUNT(ES.SkillNo) AS NumberOfSkills
FROM Employee E
	LEFT JOIN Emp_Skill ES ON E.EmpNo = ES.EmpNo
GROUP BY E.EmpName, E.Email
ORDER BY E.EmpName ASC;

--------------------7. Use SUB-QUERY technique to list out the different employees (include name, email, birthday) who are working and have multiple skills. --------------------
SELECT DISTINCT E.EmpName, E.Email, E.BirthDay
FROM Employee E
WHERE E.Status = 1
AND E.EmpNo IN (
    SELECT ES.EmpNo
    FROM Emp_Skill ES
    GROUP BY ES.EmpNo
    HAVING COUNT(ES.SkillNo) > 1
);

-------------------- 8.	Create a view to list all employees are working (include: name of employee and skill name, department name). --------------------
CREATE VIEW VWorkingEmployeesWithSkills AS
SELECT E.EmpName AS EmployeeName, S.SkillName, D.DeptName AS DepartmentName
FROM Employee E
	JOIN Emp_Skill ES ON E.EmpNo = ES.EmpNo
	JOIN Skill S ON ES.SkillNo = S.SkillNo
	JOIN Department D ON E.DeptNo = D.DeptNo
WHERE E.Status = 1;

CREATE VIEW VWorkingEmployeesWithSkills AS
SELECT E.EmpName AS EmployeeName, S.SkillName, D.DeptName AS DepartmentName
FROM (SELECT E.EmpName, E.EmpNo, E.DeptNo FROM Employee E WHERE E.Status = 1) E
	JOIN Emp_Skill ES ON E.EmpNo = ES.EmpNo
	JOIN Skill S ON ES.SkillNo = S.SkillNo
	JOIN Department D ON E.DeptNo = D.DeptNo
;

SELECT * FROM VWorkingEmployeesWithSkills;

